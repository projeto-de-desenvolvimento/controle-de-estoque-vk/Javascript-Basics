//Variáveis retornam um valor: String (texto), Number (número) ou boolean(Falso, Verdadeiro)
var nome = "Javascript";
var idade = 24;
var muitoDificilDeAprender = false;

//se a variável não existir, retorna-se um erro "Variable not defined".
//não use caracteres como ç, ´, ^, ou ~ no nome de variáveis
console.log(nome, idade, muitoDificilDeAprender);

var preco = 25;
var produtos = 5;
var precoTotal = produtos * preco;

console.log(precoTotal);

//Não é necessário repetir a palavra "var" para criar mais de uma variável, você pode usar um vírgula.
var ultimoNome = "Script",
  primeiroNome = "Java";

console.log(primeiroNome, ultimoNome);

var algo;
//Variáveis sem valor retornam undefined (indefinido).
console.log(algo);

//Variáveis declaradas depois do console retornam indefinido também
console.log(comida);

//Variáveis definidas antes da chamada de console imprimem (retornam) seu valor.
var comida = "Pizza";
console.log(comida);

//redefinindo o valor de uma variável
var meuPassatempoFavorito = "jogar Minecraft";
meuPassatempoFavorito = "Programar";
console.log(meuPassatempoFavorito);

//Exercícios para você testar no console do node

// Declare uma variável com seu nome

// Declare uma variável com sua idade

//Declare uma variável com o nome da sua comida favorita sem valor definido

//Atribua um valor para a variável anterior

//Declare 5 variáveis diferentes sem valores
