# Javascript Básico

# O que é este repositório?

##### Este repositório foi feito para ajudar iniciantes em Javascript a aprender conceitos básicos da linguagem!:computer:

# Antes de programar, algumas dicas:

##### 1. VocÊ pode usar a fonte [firacode](https://github.com/tonsky/FiraCode) para deixar seu código ainda mais.:boom: :fire:

##### 2. Você também pode usar o tema Drácula no Visual Studio Code.

# O que tem em cada pasta?

##### Cada pasta contém um tutorial de Javascript.

## Qual a ordem sugerida?

##### 1. Variáveis

##### 2. Tipos de Dados

##### 3. Números e Operadores

##### 4. Booleanos (Verdadeiro ou Falso) e Condicionais

##### 5. Funções

##### 6. Objetos

##### 7. Atribuição e Operador Ternário

##### 8. Escopo
